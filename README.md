**Concierge** is set of tools to help with the maintenance of Debian systems. 

Upon installation, the package installs a daily cron task to validate the system's configuration. 

## Goals

Notify upon issues. Keep noise to a minimum. Keep configuration to a minimum. 

## Tools

### concierge-backup

```
usage: concierge-backup [-h] [--config CONFIG] [--verify-meta]
```

Create local and remote backups of directories, and databases. 

By default, the backup include:
* Directories: /etc, /var/mail
* Databases: ejabberd, MySQL/MariaDB, and PostgreSQL

Configuration: /etc/concierge/backup.cfg

Dependency: borgbackup

### concierge-clean

```
usage: concierge-clean [-h] [--config CONFIG] [--dry-run] [--quiet]
```

Cleanup stale cache files located in the user's home directory. 

By default, prune cache files and directories not modified since 21 days. 

Configuration: optional, located in ~/.config/concierge-clean.cfg

### concierge-validate

```
usage: concierge-validate
```

Validate system configuration. 

Configuration: none

### concierge-permaudit

```
usage: concierge-permaudit [-h] [--severity {0,1,2,3,4,5,6,7}]
```

Audit filesystem permissions for possible security issues: 
* World-readable private keys (ssh, Let's Encrypt) and passwords (Git, SVN, Sympa, Dolibarr, ...)
* World-writable configuration files and scripts (/etc/init.d/*, /etc/profile, ...)
* World-writable executable search path (ie $PATH), or perl/python/ruby search path
* Process running a world-writable executable, or world-writable bash/perl/python script
* Sensitive information stored in the wrong place (passwords in /etc/passwd rather than /etc/shadow)

This tool only does file permissions checks, and does it imperfectly. 
You should not rely on this single tool for security auditing. 

Configuration: none

### concierge-status

```
usage: concierge-status
```

Check system status.

Configuration: none
