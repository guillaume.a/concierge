# Releasing

## Debian changelog

- `date -R`

## Debian packaging

1. cd concierge-x.y
2. nano debian/changelog
3. dh_make -i -c gpl3 --createorig
4. debuild -us -uc
